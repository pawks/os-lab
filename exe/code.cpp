#include<iostream>
#include<unistd.h>
using namespace std;

int main()
{
    int i=0;
    while(i<2)
        if(fork()==0)
        {
            switch(i)
            {
                case 0:
                    execlp("ls","ls","-al",NULL);
                    break;
                case 1:
                    char *args[]={"ps","-a","-N",NULL};
                    execvp(args[0],args);
                    break;
            }
        }
        else
            ++i;
    cout<<endl;
    return 0;
} 