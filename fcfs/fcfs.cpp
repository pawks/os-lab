#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
struct jobs{
        int pid,at,bt,ct,tat,wt;
};

bool comparator(jobs j1,jobs j2){return (j1.at<j2.at);}

class fcfs{
    vector <jobs> queue;
    float avg_wt;
    int n;
  public:
    void input()
    {
        cout<<"Enter the total number of jobs:";
        cin>>n;
        cout<<"Enter the jobs in the following format(pid,at,bt):\n";
        for(int i=0;i<n;++i)
        {
            jobs temp;
            cin>>temp.pid>>temp.at>>temp.bt;
            temp.ct=0;
            temp.tat=0;
            temp.wt=0;
            queue.push_back(temp);
        }
        avg_wt=0;
    }
    void schedule()
    {
        sort(queue.begin(),queue.end(),comparator);
        queue[0].ct=queue[0].bt+queue[0].at;
        queue[0].tat=queue[0].ct-queue[0].at;
        queue[0].wt=0;
        int prev=queue[0].ct;
        for(int i=1;i<n;++i)
        {
            queue[i].ct=queue[i].bt+prev;
            queue[i].tat=queue[i].ct-queue[i].at;
            queue[i].wt=queue[i].tat-queue[i].bt;
            prev=queue[i].ct;
            avg_wt+=queue[i].wt;
        }
    }
    void output()
    {
        cout<<"PID\tAT\tBT\tCT\tTAT\tWT\n";
        for(int i=0;i<n;++i)
        {
            cout<<queue[i].pid<<"\t"<<queue[i].at<<"\t"<<queue[i].bt;
            cout<<"\t"<<queue[i].ct<<"\t"<<queue[i].tat<<"\t"<<queue[i].wt;
            cout<<endl;
        }
        cout<<"\nAvg:"<<avg_wt/n<<endl;
    }
};
int main()
{
    fcfs obj;
    obj.input();
    obj.output();
    obj.schedule();
    obj.output();
    return 0;   
}