#include<iostream>
#include<unistd.h>
#include<vector>
#include<string>
#include<sstream>
#include<string.h>
#include<wait.h>    
using namespace std;
int main()
{
    vector<string> history;
    string line;
    char *args[128];
    std::cout.setf(std::ios::unitbuf);
    while(1)
    {
        cout<<"\nMyShell:";
        if(fork()==0)
            execlp("pwd","pwd",NULL);
        else
            wait(NULL);
        cout<<"$";
        getline(std::cin,line);
        //cout<<"Entered:"<<line<<endl;
        if(line[0]=='!')
            if(line[1]=='!'){
                history.clear();
                continue;
            }
            else
            {
                int ind;
                line.erase(0,1);
                ind=stoi(line,nullptr);
                ind=history.size()-ind;
                if(ind<0){
                    cout<<"Option Error";
                    continue;
                }
                else
                    line=history[ind];
                //cout<<endl<<line;
            }
        if(line.find("exit")==0)
            exit(0);    
        history.push_back(line);
        if(history.size()==6)
            history.erase(history.begin());
        int i=0;
        std::stringstream ss(line);
        ss >> std::noskipws;
        std::string field;
        char ws_delim;
        while(1) 
        {
            if( ss >> field ){
                char *temp=new char[field.length()+1];
                strcpy(temp,field.c_str());
                args[i++]=temp;
            }
            else if (ss.eof())
                break;
            else
                args[i++]=NULL;
            ss.clear();
            ss >> ws_delim;
        }
        args[i]=NULL;
        if(fork()==0){
            execvp(args[0],args);
            cout<<"\nError wrong command\n";
            exit(0);
        }
        else
            wait(NULL);
    }
}