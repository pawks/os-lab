#include<iostream>
#include<unistd.h>
#include<sys/wait.h>
#include<algorithm>
using namespace std;
void reaper(int signum)
{
    wait(NULL);
    exit(0);
}
void bsearch(int *arr,int start,int end,int element)
{
    int mid=(start+end)/2;
    if(start>end)
    {
        cout<<"Not found\n";
        return;
    }
    if(arr[mid]==element)
    {
        cout<<"Found"<<endl;
        return;
    }
    else if(arr[mid]<element)
            bsearch(arr,mid+1,end,element);
    else 
            bsearch(arr,start,mid-1,element);
}
int main()
{
    int *arr;
    int n,element;
    cout<<"Enter the size:\n";
    cin>>n;
    cout<<n<<endl;
    arr=new int[n];
    cout<<"Enter the elements\n";
    for(int i=0;i<n;++i)
        cin>>arr[i];
    sort(arr,arr+n);
    for(int i=0;i<n;++i)
        cout<<arr[i]<<"\t";
    cout<<endl;
    cout<<"Enter the element to search for:\n";
    cin>>element;
    bsearch(arr,0,n-1,element);
    if(fork==0)
        bsearch(arr,0,n-1,element);
    else    
    {
        signal(SIGCHLD,reaper);
        while(1);
    }
}