#include<unistd.h>
#include<iostream>
using namespace std;
int main()
{
    if(fork()==0)
    {
        cout<<"Orphan process"<<getpid()<<"\t"<<getppid()<<endl;
        sleep(30);
        cout<<"Orphan process"<<getpid()<<"\t"<<getppid()<<endl;
    }
    else
        return 0;
}
