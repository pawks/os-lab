#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct process{

	int pid, at, bt;
	int tat, ct, wt; 
};

class srt
{
	vector <process> a;
	int n;
  public :
	void input();
	void output();
	static bool cmp(process &x, process &y)
	{   return (x.at < y.at); }
	static bool cmp2(process &x, process &y)
	{   return (x.bt < y.bt); }	
	static bool cmp3(process &x, process &y)
	{   return (x.pid < y.pid); }			
};

void srt::input()
{
  cout<<"Enter number of process\n";
  cin>>n;
  cout<<"Enter the process id, arrival time and burst time\n";
  process x;
  for(int i =0; i<n; ++i)
  {
	cin>>x.pid>>x.at>>x.bt;
	a.push_back(x);
  }
}	


void srt::output()
{
  vector <process> b, f, e;
  b = a;
  process x;
  sort(b.begin(), b.end(), cmp);
  int t, i, count = 0, min, j;
  t = b[0].at;
  int maxt = b[b.size()-1].at;
  while(b.size() != 0)
  {
	if(t >= maxt)
		break;
	sort(b.begin(), b.end(), cmp);
	min = b[0].bt;
	j = 0; 
	for(i=0;i<b.size();i++)	
		if(min > b[i].bt && b[i].at <= t)
			{
				min = b[i].bt;
				j = i;
			}
	for(i=0;i<b.size();++i)
		if(b[i].at > t)
			break;
	if(t + b[j].bt > b[i].at)
	{
		b[j].bt -= (b[i].at - t);
		t = b[i].at;
		f.push_back(b[j]);
	} 
	else
	{
		t += b[i].bt;
		b[j].bt = 0;
		b[j].ct = t;
		b[j].tat = b[j].ct - b[j].at;
		e.push_back(b[j]);
		f.push_back(b[j]);
		b.erase(b.begin() + j);
	}
	count++;
 	
  }
  sort(b.begin(), b.end(), cmp2);
  for(i=0;i<b.size();++i)
  {
	b[i].ct = t + b[i].bt;
	t += b[i].bt;
	b[i].tat = b[i].ct - b[i].at; 
  }
  for(auto x: e)
	b.push_back(x);
  sort(b.begin(), b.end(), cmp3);
  sort(a.begin(), a.end(), cmp3);
  for(i=0;i<b.size();++i)
 {
		b[i].at = a[i].at;
		b[i].bt = a[i].bt;
		b[i].wt = b[i].tat - b[i].bt;
	
 }
  a = b ;
  sort(a.begin(), a.end(), cmp);
  cout<<"\nPID  -  AT  - BT   - CT  - TAT  - WT \n";	
  for(i = 0;i<n;++i)
	cout<<a[i].pid<<" "<<a[i].at<<" "<<a[i].bt<<" "<<a[i].ct<<" "<<" "<<a[i].tat<<" "<<" "<<a[i].wt<<"\n";
  int ti = 0;
  for(i = 0;i<n;++i)
	ti += a[i].wt;
  cout<<"Average WT = "<<ti/(n*1.0);
}



int main()
{
  class srt b;
  b.input();
  cout<<"\n";
  b.output(); 
}

/*
	6
	1 6 1 
	2 3 3 
	3 4 6 
	4 1 5 
	5 2 2 
	6 5 1 

4
1 0 8
2 1 4
3 2 9
4 3 5

*/
