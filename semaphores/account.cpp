#include<iostream>
#include<semaphore.h>
#include<pthread.h>
#include<unistd.h>

using namespace std;

sem_t counter;

class acc{
        int ac_number;
        float bal;
        float minbal_per;
        pthread_mutex_t mut;
        bool check_min_bal(float val);
    public:
        void inc_bal(float val);
        bool dec_bal(float val);
        float ret_bal();
        int ret_ac_num();
};
bool acc::check_min_bal(float val)
{
    return ((bal*minbal_per/100)<=(bal-val));
}
void acc::inc_bal(float val)
{
    bal=bal+val;
}
bool acc::dec_bal(float val)
{
    bool flag=check_min_bal(val);
    if(flag)
        bal=bal-val;
    return flag;
}
float acc::ret_bal()
{
    return bal;
}
int acc::ret_ac_num()
{
    return ac_number;
}