#include<iostream>
#include<semaphore.h>
#include<pthread.h>
#include<unistd.h>

using namespace std;

sem_t mutex,cusDone,barberReady,customerReady,chair_mutex;
int n,c;

void get_haircut(int *x)
{
    cout<<*x<<" started getting a haircut\n";
    sleep(2);
    cout<<*x<<" finished getting a haircut\n";
}

void *barber(void *)
{
    while(1)
    {
        sem_wait(&customerReady);
        sem_post(&barberReady);
        sem_wait(&cusDone);
    }
}

void leave_shop(int *x)
{
    cout<<*x<<" leaving shop\n";
    c-=1;    
}

void get_chair(int *x)
{
    if(c==n)
    {
        sem_post(&chair_mutex);
        cout<<"No chairs "<<*x<<" exiting\n";
        pthread_exit(NULL);
    }
    c+=1;
    cout<<*x<<" Waiting for barber\n";
}

void customer(int *x)
{
    cout<<*x<<" entered shop\n";
    sem_wait(&chair_mutex);
    get_chair(x);
    sem_post(&chair_mutex);
    //sem_wait(&mutex);
    //cout<<*x<<"accmut\n";
    sem_post(&customerReady);
    sem_wait(&barberReady);
    get_haircut(x);
    sem_post(&cusDone);
    //sem_post(&mutex);
    sem_wait(&chair_mutex);
    leave_shop(x);
    sem_post(&chair_mutex);
    pthread_exit(NULL);
}

inline void *customer_wrapper(void *args)
{
    int *x=(int *)args;
    customer(x);
}

int main()
{
    std::cout.setf(std::ios::unitbuf);
    int n_cust;
    cout<<"Enter the maximum number of customers:";
    cin>>n;
    cout<<"Enter the number of customers:";
    cin>>n_cust;
    sem_init(&cusDone,0,0);
    sem_init(&chair_mutex,0,1);
    //sem_init(&mutex,0,1);
    sem_init(&customerReady,0,0);
    sem_init(&barberReady,0,0);
    pthread_t *tid;
    int **args;
    args=new int*[n_cust];
    for(int i=0;i<n_cust;++i)
        args[i]=new int;
    tid=new pthread_t[n_cust+1];
    pthread_create(&tid[0],NULL,barber,NULL);
    for(int i=0;i<n_cust;++i)
    {
        *args[i]=i;
        pthread_create(&tid[1+i],NULL,customer_wrapper,(void *)args[i]);
    }
    for(int i=0;i<n_cust;++i)
        pthread_join(tid[i+1],NULL);
    sem_destroy(&cusDone);
    sem_destroy(&chair_mutex);
    //sem_destroy(&mutex);
    sem_destroy(&customerReady);
    sem_destroy(&barberReady);
    exit(0);
}