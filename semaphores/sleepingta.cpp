#include<iostream>
#include<semaphore.h>
#include<pthread.h>
#include<unistd.h>
#include<random>

using namespace std;

sem_t cusDone,barberReady,customerReady,chair_mutex,print_mut;
int n,c;
random_device seeder;
mt19937 rng(seeder());
uniform_int_distribution<int> gen(1,30);

void leave_shop(int *x)
{
    c-=1;    
}

void get_haircut(int *x)
{
    sem_wait(&chair_mutex);
    leave_shop(x);
    sem_post(&chair_mutex);
    cout<<*x<<" started getting help\n";
    sleep(2);
    cout<<*x<<" finished getting help\n";
}

void *barber(void *)
{
    while(1)
    {
        sem_wait(&customerReady);
        sem_post(&barberReady);
        sem_wait(&cusDone);
    }
}

int get_chair(int *x)
{
    if(c==n)
    {
        sem_post(&chair_mutex);
        sem_wait(&print_mut);
        cout<<"No chairs for "<<*x;
        sem_post(&print_mut);
        return gen(rng);
    }
    c+=1;
    sem_post(&chair_mutex);
    sem_wait(&print_mut);
    cout<<*x<<" Waiting for ta\n";
    sem_post(&print_mut);
    return -1;
}

void customer(int *x)
{
    while(1){
        sem_wait(&print_mut);
        cout<<*x<<" entered lab\n";
        sem_post(&print_mut);
        sem_wait(&chair_mutex);
        int r=get_chair(x);
        if(r==-1){
            sem_post(&customerReady);
            sem_wait(&barberReady);
            get_haircut(x);
            sem_post(&cusDone);
            sem_wait(&print_mut);
            cout<<*x<<" done and exiting"<<endl;
            sem_post(&print_mut);
            pthread_exit(NULL);
        }
        else
        {
            sem_wait(&print_mut);
            cout<<*x<<" returning after "<<r<<endl;
            sem_post(&print_mut);
            sleep(r);
        }
    }
}

inline void *customer_wrapper(void *args)
{
    int *x=(int *)args;
    customer(x);
}

int main()
{
    std::cout.setf(std::ios::unitbuf);
    int n_cust;
    cout<<"Enter the maximum number of customers:";
    cin>>n;
    cout<<"Enter the number of customers:";
    cin>>n_cust;
    sem_init(&cusDone,0,0);
    sem_init(&chair_mutex,0,1);
    sem_init(&print_mut,0,1);
    sem_init(&customerReady,0,0);
    sem_init(&barberReady,0,0);
    pthread_t *tid;
    int **args;
    args=new int*[n_cust];
    for(int i=0;i<n_cust;++i)
        args[i]=new int;
    tid=new pthread_t[n_cust+1];
    pthread_create(&tid[0],NULL,barber,NULL);
    for(int i=0;i<n_cust;++i)
    {
        *args[i]=i;
        pthread_create(&tid[1+i],NULL,customer_wrapper,(void *)args[i]);
    }
    for(int i=0;i<n_cust;++i)
        pthread_join(tid[i+1],NULL);
    sem_destroy(&cusDone);
    sem_destroy(&chair_mutex);
    sem_destroy(&customerReady);
    sem_destroy(&barberReady);
    exit(0);
}