#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
using namespace std;

int det(vector <vector <int>> a, int n)
{
	if(n == 2)
	{
		return a[0][0]*a[1][1] - a[0][1]*a[1][0];		
	}
	else
	{
		int i = 0, j, k, l, f, g;
		int val = 1, sum = 0;
		for(j=0;j<n; ++j)
		{
			vector <vector <int>> b;
			b.resize(n-1, vector <int>(n-1));
			f = 0;
			g = 0;
			for(l=0;l<n;++l)			
			{		
				g = 0;
				for(k=0;k<n;++k)
					if(l != i && k != j)
						b[f][g++] = a[l][k];
				if(l != i)
					f++;	
			}
 
			sum += val*a[i][j]*det(b, n-1);
			val *= -1;
		}
		return sum;
	}
}

void rdet(vector <vector <int>> &a, int &detr, int n)
{
	detr = det(a, n);
}
vector<vector <int>> minors(vector<vector<int>> a, int n)
{
	vector <vector <int>> m;
	m.resize(n, vector <int>(n));
	int i, j, k, l, f, g;
	for(i=0;i<n;++i)
	{
		for(j=0;j<n;++j)
		{
			vector <vector <int>> x; 
			x.resize(n-1, vector <int>(n-1));
			f = 0;
			for(l=0;l<n;++l)
			{
				g = 0;
				for(k=0;k<n;++k)
					if(k != j && l != i)
						x[f][g++] = a[l][k];
				if(l != i)
					f++;
			}
			m[i][j] = det(x, n-1);						
		}
	}
	return m;
}

vector<vector <int>> cofactor(vector<vector<int>> b, int n)
{
	vector <vector <int>> a = minors(b, n);
	int val = 1;
	for(int i = 0;i <n; ++i)
	{	for(int j=0;j<n;++j)
		{	
			a[i][j] *= val; 
			val *= -1;
		}
	}
	return a;	
}

vector<vector <int>> transpose(vector<vector<int>> a, int n, int m)
{
	vector <vector <int>> x;
	x.resize(n, vector <int>(m));
	for(int i = 0;i<m; ++i)
		for(int j=0;j<n;++j)
			x[j][i] = a[i][j];
	return x;
}
void adjoint(vector <vector <int>> &b, vector <vector <int>> &a, int n)
{
	b = transpose(cofactor(a, n), n, n);
}

vector<vector <float>> inverse(vector <vector <int>> a, int n)
{
	vector <vector <int>> adj = a;
	vector <vector<float>> inv;
	inv.resize(n, vector<float>(n));
	int x;
	thread l(&adjoint, std::ref(adj), std::ref(a), n);
	thread r(&rdet, std::ref(a), std::ref(x), n);

	l.join();
	r.join();        
	for(int i = 0; i <n; ++i)
		for(int j=0;j<n; ++j)
			inv[i][j] = adj[i][j]*1.0/x; 	
	return inv;	 
}
int main()
{
	int i, j, n;
	//vector <vector <int>> a = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
	//vector <vector <int>> a = {{7, 2, 1}, {0, 3, -1}, {-3, 4, -2}};	
	cout<<"Enter the size of array\n";
	cin>>n;	
	cout<<"Enter the elements of the array\n";
	vector <vector <int>> a(n, vector<int>(n));
	for(i=0;i<n;i++)
	{	for(j=0;j<n;++j)
			cin>>a[i][j];
	}
	cout<<"\n";
	for(i=0;i<n;i++)
	{	for(j=0;j<n;++j)
			cout<<a[i][j]<<" ";
		cout<<"\n";
	}
	cout<<"\nDeterminant = "<<det(a, n)<<"\n";
	if(det(a, n) != 0)
	{		
		vector <vector<float>> b = inverse(a, n);
		for(i=0;i<n;i++)
		{	for(j=0;j<n;++j)
				cout<<b[i][j]<<" ";
			cout<<"\n";
		}
	}
	else
	{
		cout<<"Inverse doesn't exist\n";
	}
	return 0;
}
