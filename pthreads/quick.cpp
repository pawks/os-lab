#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>

using namespace std;
void runner(vector <int> &a, int beg, int end); 

int qpartition(vector <int> &a, int beg, int stop)
 {

    int temp, j = stop, i = beg+1;
    while(i<j)
    {
        while(a[i] < a[beg])
            i++;
        while(a[j] > a[beg])
            j--;
        if(i < j)
       {
        temp = a[i];
        a[i] = a[j];
        a[j] = temp;
       }
    }
        temp = a[j];
        a[j] = a[beg];
        a[beg] = temp;
    return j;
 }

void quicksort(vector <int> &a, int beg, int end)
{
	if(beg < end)
	{
		int p = qpartition(a, beg, end);
		thread left(&runner, std::ref(a), beg, p-1);
		thread right(&runner, std::ref(a), p+1, end);
		left.join();
		right.join();
	}
	
}

int main()
{
	int n;
	cout<<"Size\n";
	cin>>n;	
	vector <int> a(n);
	cout<<"Array\n";
   	for(int i = 0;i<n;++i)
		cin>>a[i];
	quicksort(a, 0, a.size()-1);
	for(auto x: a)
	cout<<x<<" ";	
	return 0;
}

void runner(vector <int> &a, int beg, int end)
{
	quicksort(a, beg, end);
}

