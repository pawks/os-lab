#include <iostream>
#include <pthread.h>
#include<random>
#include "monte_carlo_pi_macros.h"

using namespace std;

random_device seeder;
mt19937 rng(seeder());
uniform_real_distribution<float> gen(0, 1);


struct struct_args{
    int *iter,*val;
};

void monte_carlo_sim(int *iter,int *val)
{
    *val=0;
    //cout<<val<<"\t"<<*val<<endl;
    for(int i=0;i<*iter;++i)
    {
        float x= gen(rng);
        float y= gen(rng);
        //cout<<x<<"\t"<<y<<endl;
        if((x*x+y*y)<=1)
            ++*val;
        //cout<<val<<"\t"<<*val<<endl;
    }
    cout<<pthread_self()<<"\t"<<(float)*val/(*iter-*val)<<endl;
    pthread_exit(0);
}

inline void *wrapper(void *args)
{
    struct_args *arg=(struct_args *) args;
    //cout<<"h"<<arg->val<<endl;
    monte_carlo_sim(arg->iter,arg->val);
    
}

int main()
{
    int *val;
    val=new int[num_threads];
    pthread_t *tid;
    tid=new pthread_t[num_threads];
    struct_args *args;
    args=new struct_args[num_threads];
    for(int i=0;i<num_threads;++i)
    {
        (args[i].iter)=new int;
        *(args[i].iter)=num_iter/num_threads;
        (args[i].val)=val+i;
        //cout<<(args[i].val)<<endl;
        pthread_create(&tid[i],NULL,wrapper,(void *)(args+i));
        //pthread_join(tid[i],NULL);
    }
     for(int i=0;i<num_threads;++i)
        pthread_join(tid[i],NULL);
    for(int i=1;i<num_threads;++i)
        val[0]+=val[i];
    cout<<"PI:"<<(float)val[0]/(num_iter-val[0])<<endl;
    return 0;
}