#include <iostream>
using namespace std;
int c[100];

struct parpass{

	int *a;
	int beg, end;
};

void * runner(void *params);
 void merge(int a[], int beg, int end)
{
 int i, j, k = 0;
 i = beg;
 int mid = (beg+end)/2;
 j = mid + 1;
 while(i <= mid && j <= end)
 {
     if(a[i] > a[j])
        c[k++] = a[j++];
     else
        c[k++] = a[i++];
 }
 while(i <= mid)
     c[k++] = a[i++];

 while(j <= end)
    c[k++] = a[j++];
 int y = beg;
 for(i =0; i< k; i++)
   {
    a[y++] = c[i];
   }
}
 void mergesort(int a[], int beg, int end)
 {
     if(beg<end)
     {	
	pthread_t tid[2];       
	pthread_attr_t attr; 
    pthread_attr_init(&attr);
	parpass *x = new parpass;
	x->a = a;
	x->beg = beg;
	x->end = (beg+end)/2;
	
        pthread_create(&tid[0],&attr,runner,x);
	
	
	parpass *y = new parpass;
	y->a = a;
	y->beg = (beg+end)/2 + 1;
	y->end = end;
	
	pthread_create(&tid[1], &attr, runner, y);

	pthread_join(tid[0], NULL);
	pthread_join(tid[1], NULL);
        
	merge(a, beg, end);
     }
 }



int main()
{
    int n;
    cout<<"Size\n";
    cin>>n;
    int b[n]; 
    cout<<"Array\n";
    for(int i = 0;i<n;++i)
	cin>>b[i];
    mergesort(b, 0, n-1);	
    for(int i =0;i<n;++i)
	cout<<b[i]<<" ";
    return 0;
}

 void *runner(void * x)
{
	parpass *params = (parpass *)x; 
	mergesort(params->a, params->beg, params->end);
}
