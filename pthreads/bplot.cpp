#include<pthread.h>
#include<iostream>
#include<stdlib.h>
#include<algorithm>

using namespace std;

struct struct_args
{
    int *arr;
    int size;
    float *lowerquartile;
    float *upperquartile;
    float *median;
    float *min,*max;
};

float median(int *arr,int size)
{
    if(size%2==0)
        return (float)(arr[size/2]+arr[size/2-1])/2;
    else
        return arr[(size-1)/2];
        
    pthread_exit(0);
}
inline void *median_wrapper(void *args)
{
    struct_args *arg=(struct_args *) args;
    sort(arg->arr,arg->arr+arg->size);
    *(arg->min)=(float)*(arg->arr);
    *(arg->max)=(float)*(arg->arr+(arg->size-1)); 
    *(arg->median)=median( arg->arr, arg->size);
    cout<<"Min : "<<*(arg->min)<<endl;
    cout<<"Max : "<<*(arg->max)<<endl;
    cout<<"Median : "<<*(arg->median)<<endl;
    pthread_exit(0);
}

inline void *lowerquartile_wrapper(void *args)
{
    struct_args *arg=(struct_args *)args;
    int c=1;
    for(int i=0;i<arg->size;++i)
        if(arg->arr[i]<*(arg->median))
            ++c;
    *(arg->lowerquartile)=median(arg->arr,c);
    cout<<"Lower Quartile : "<<*(arg->lowerquartile)<<endl;
    pthread_exit(0);
}

inline void *upperquartile_wrapper(void *args)
{
    struct_args *arg=(struct_args *)args;
    int c=0;
    int *tarr;
    tarr=new int[arg->size];
    for(int i=0;i<arg->size;++i)
        if(arg->arr[i]>*(arg->median))
        {
            tarr[c]=arg->arr[i];
            ++c;
        }
    *(arg->upperquartile)=median(tarr,c+1);
    cout<<"Upper Quartile : "<<*(arg->upperquartile)<<endl;
    pthread_exit(0);
}
int main(int argc,char *argv[])
{
    struct_args *args;
    args=new struct_args;
    args->size=argc-1;
    args->arr=new int[argc-1];
    args->median=new float;
    args->min=new float;
    args->max=new float;
    args->lowerquartile=new float;
    args->upperquartile=new float;
    for(int i=0;i<args->size;++i)
        args->arr[i]=(int)strtol(argv[i+1],NULL,0);
    for(int i=0;i<args->size;++i)
        cout<<args->arr[i]<<"\t";
    cout<<endl;
    pthread_t median_id,lq_id,uq_id;
    pthread_create(&median_id,NULL,median_wrapper,(void *)args);
    pthread_join(median_id,NULL);
    pthread_create(&lq_id,NULL,lowerquartile_wrapper,(void *)args);
    pthread_create(&uq_id,NULL,upperquartile_wrapper,(void *)args);
    pthread_join(lq_id,NULL);
    pthread_join(uq_id,NULL);
    return 0;
}