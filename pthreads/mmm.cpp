#include<pthread.h>
#include<iostream>
#include<stdlib.h>
#include<algorithm>

using namespace std;

struct struct_args
{
    int *arr;
    int size;
};

void mean(int *arr,int size)
{
    float sum=0;
    for(int i=0;i<size;++i)
        sum+=arr[i];
    sum/=size;
    cout<<"Mean : "<<sum<<endl;
    pthread_exit(0);
}

void median(int *arr,int size)
{
    sort(arr,arr+size);
    if(size%2==0)
        cout<<"Median : "<<(float)(arr[size/2]+arr[size/2-1])/2<<endl;
    else
        cout<<"Median : "<<arr[(size-1)/2]<<endl;
    pthread_exit(0);
}

void mode(int *arr,int size)
{
    sort(arr,arr+size);
    int number = arr[0];
    int mode = number;
    int count = 1;
    int countMode = 1;
    for (int i=1; i<size; i++)
    {
        if (arr[i] == number) 
        { // count occurrences of the current number
            ++count;
        }
        else
        { // now this is a different number
            if (count > countMode) 
            {
                countMode = count; // mode is the biggest ocurrences
                mode = number;
            }
            count = 1; // reset count for the new number
            number = arr[i];
        }
    }
    cout << "Mode : " << mode << endl;
    pthread_exit(0);
}

inline void *mean_wrapper(void *args)
{
    struct_args *arg=(struct_args *) args; 
    mean( arg->arr, arg->size);
}
inline void *median_wrapper(void *args)
{
    struct_args *arg=(struct_args *) args;
    median( arg->arr, arg->size);
}
inline void *mode_wrapper(void *args)
{
    struct_args *arg=(struct_args *) args;
    mode( arg->arr, arg->size);
}
int main(int argc,char *argv[])
{
    struct_args *args;
    args=new struct_args;
    args->size=argc-1;
    args->arr=new int[argc-1];
    for(int i=0;i<args->size;++i)
        args->arr[i]=(int)strtol(argv[i+1],NULL,0);
    for(int i=0;i<args->size;++i)
        cout<<args->arr[i]<<"\t";
    cout<<endl;
    pthread_t mean_tid,median_tid,mode_tid;
    pthread_create(&mean_tid,NULL,mean_wrapper,(void *) args);
    pthread_create(&median_tid,NULL,median_wrapper,(void *) args);
    pthread_create(&mode_tid,NULL,mode_wrapper,(void *) args);
    pthread_join(mean_tid,NULL);
    pthread_join(median_tid,NULL);
    pthread_join(mode_tid,NULL);
}