#include<pthread.h>
#include<iostream>
#include<stdlib.h>

using namespace std;

void primenumber(int number)
{
    for(int i=2;i<=number/2;++i)
        if(number%i==0)
            pthread_exit(0);
    cout<<number<<endl;
    pthread_exit(0);
}
inline void *primenumbergen(void *attr)
{
    primenumber(*(int *) attr);
}

int main(int argc,char *argv[])
{
    int val=(int)strtol(argv[1],NULL,0);
    pthread_t *tid;
    tid= new pthread_t[val];
    int *arg=new int;
    cout<<"Prime Numbers till "<<val<<":"<<endl;
    for(int i=1;i<=val;++i)
    {
        *arg=i;
        pthread_create(&tid[i-1],NULL,primenumbergen,(void *)arg);
    }
    for(int i=0;i<val;++i)
        pthread_join(tid[i],NULL);
    return 0;
}