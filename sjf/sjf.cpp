#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
struct jobs{
        int pid,at,bt,ct,tat,wt;
};
bool comparator(jobs j1,jobs j2){return (j1.at<j2.at);}
class sjf{
    vector <jobs> queue;
    float avg_wt;
    int n;
  public:
    void input()
    {
        cout<<"Enter the total number of jobs:";
        cin>>n;
        cout<<"Enter the jobs in the following format(pid,at,bt):\n";
        for(int i=0;i<n;++i)
        {
            jobs temp;
            cin>>temp.pid>>temp.at>>temp.bt;
            temp.ct=0;
            temp.tat=0;
            temp.wt=0;
            queue.push_back(temp);
        }
        avg_wt=0;
    }
    int ret_sj(int k)
    {
        int sj,si=-1;
        sj=9999;
        for(int i=0;i<n;++i)
        {
            if((queue[i].at<=k)&&(queue[i].bt<sj)&&(queue[i].ct==0))
            {
                sj=queue[i].bt;
                si=i;
            }
        }
        return si;
    }
    void schedule()
    {
        int prev=0,si,i=0;
        while(i<n)
        {
            si=ret_sj(prev);
            //cout<<si<<endl;
            if(si!=-1){
            queue[si].ct=queue[si].bt+prev;
            queue[si].tat=queue[si].ct-queue[si].at;
            queue[si].wt=queue[si].tat-queue[si].bt;
            prev=queue[si].ct;
            ++i;
            avg_wt+=queue[si].wt;
            }
            else
                prev=prev+1;
        }

    }
    void output()
    {
        cout<<"PID\tAT\tBT\tCT\tTAT\tWT\n";
        for(int i=0;i<n;++i)
        {
            cout<<queue[i].pid<<"\t"<<queue[i].at<<"\t"<<queue[i].bt;
            cout<<"\t"<<queue[i].ct<<"\t"<<queue[i].tat<<"\t"<<queue[i].wt;
            cout<<endl;
        }
        cout<<"\nAvg:"<<avg_wt/n<<endl;
    }
};
int main()
{
    sjf obj;
    obj.input();
    obj.output();
    obj.schedule();
    obj.output();
    return 0;   
}