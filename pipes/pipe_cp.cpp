#include <iostream>
#include <unistd.h>
#include <wait.h>
#include <algorithm>
#include <vector>
#include <sstream>
#include <string.h>
#include <fstream>
#define REND 0
#define WEND 1
#define size 25

using namespace std;

void bipipe()
{
	int fd1[2], fd2[2];

	// Get output file name from stdin as a string
	cout<<"Dest file:";			
	string out;
	getline(std::cin, out);

	// Get input file name from stdin as a string
	cout<<"Src file:";			
	string in;
	getline(std::cin, in);

	if(pipe(fd1) >= 0 && pipe(fd2) >= 0)
	{
		pid_t pid;
		pid = fork();
		if(pid == 0)
		{	
			char rm[size];
			int x;

			FILE *wptr;			
			wptr = fopen(out.c_str(), "w");	
		
			close(fd1[WEND]);
			close(fd2[REND]);
			// Read rm from pipe(fd1) and write it to destination file
			
			while(read(fd1[REND], rm, sizeof(rm)) > 0 )
			{
				fprintf(wptr, "%s ", rm);	
			}


			close(fd1[REND]);
			close(fd2[WEND]);

			exit(0);
		}
		else if(pid > 0)	
		{
			
			int x;	
			FILE *rptr;	
						
			rptr = fopen(in.c_str(), "r");			
			close(fd1[REND]);
			// Write text from file to pipe 
			while(!feof(rptr) || x > 0)
			{	
				char rm[size];
				if(x = fscanf(rptr, "%s", rm) > 0 )	
					write(fd1[WEND], rm, sizeof(rm));

			}
			
			close(fd1[WEND]);
			
			close(fd2[WEND]);			
			close(fd2[REND]);
			wait(0);
		}
		else
			cout<<"Error in process creation\n";
	
	}
	else
		cout<<"Error in pipe creation\n";
}


int main()
{
	std::cout.setf(std::ios::unitbuf);
	std::cin.setf(std::ios::unitbuf);
	bipipe();
}



