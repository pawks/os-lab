#include <iostream>
#include <unistd.h>
#include <wait.h>
#include <algorithm>
#include <vector>
#include <sstream>

#define REND 0
#define WEND 1
#define size 25

using namespace std;
void bipipe()
{
	int fd1[2], fd2[2];
	cout<<"Bi directional pipe\n";
	if(pipe(fd1) >= 0 && pipe(fd2) >= 0)
	{
		pid_t pid;
		pid = fork();
		if(pid == 0)
		{	
			char rm[size];
			char wm[size] = "BIPIPE";
			close(fd1[WEND]);
			read(fd1[REND], rm, sizeof(rm));
			fprintf(stdout, "%s %s\n", "Message in child : ", rm);
			close(fd1[REND]);
			
			close(fd2[REND]);
			write(fd2[WEND], wm, sizeof(wm));
			close(fd2[WEND]);
			exit(0);
		}
		else if(pid > 0)	
		{
			char rm[size];
			char wm[size] = "message";
			close(fd1[REND]);
			write(fd1[WEND], wm, sizeof(wm));
			close(fd1[WEND]);
			
			close(fd2[WEND]);
			read(fd2[REND], rm, sizeof(rm));
			fprintf(stdout, "%s %s\n", "Message in PARENT : ", rm);			
			close(fd2[REND]);			
			wait(0);
		}
		else
			cout<<"Error in process creation\n";
	
	}
	else
		cout<<"Error in pipe creation\n";
}
void unipipe()
{
	int fd[2];
	cout<<"Uni directional pipe\n";
	if(pipe(fd) >= 0)
	{
		pid_t pid;
		pid = fork();
		if(pid == 0)
		{	
			char wm[size];
			close(fd[WEND]);
			read(fd[REND], wm, sizeof(wm));
			fprintf(stdout, "%s %s \n", "Message : ", wm);
			close(fd[REND]);
			exit(0);
		}
		else if(pid > 0)	
		{
			char rm[size] = "UNIPIPE";
			close(fd[REND]);
			write(fd[WEND], rm, sizeof(rm));
			close(fd[WEND]);
			wait(0);		
		}
		else
			cout<<"Error in process creation\n";
	
	}
	else
		cout<<"Error in pipe creation\n";
}

int main()
{
	
	unipipe();
	bipipe();
}
