#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    char *args[]={"tr","[:lower:]","[:upper:]",NULL};
    pid_t pid;
    int pipefd[2];
    int status;
    pipe(pipefd);
    pid = fork();
    if (pid == 0) {
        dup2(pipefd[1], STDOUT_FILENO);
        close(pipefd[0]);
        close(pipefd[1]);
        if (execlp("ls", "ls", NULL) == -1)
            perror("execl");
    } else {
        wait(&status);
        if (fork() == 0) {
            dup2(pipefd[0], STDIN_FILENO);
            close(pipefd[0]);
            close(pipefd[1]);
            if (execvp(args[0],args) == -1)
                perror("execvp grep");
        } else {
            wait(&status);
            close(pipefd[0]);
            close(pipefd[1]);
            
        }
    }
}