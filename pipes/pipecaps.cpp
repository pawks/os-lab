#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>
#include <iostream>
#include <ctype.h>
#include <string.h>
using namespace std;
 

#define MAX 512
int main()
{
    char buf[MAX];
    int pipefd1[2],nb;
    if(pipe(pipefd1)>=0)
        cout<<"pipecreated"<<endl;
    if(fork()!=0)
    {
        cout<<"Enter the string:"<<endl;
        cin>>buf;
        write(pipefd1[1],buf,strlen(buf));
        close(pipefd1[0]);
        wait(NULL);
        exit(1);
    }
    else
    {
        close(pipefd1[1]);
        while ((nb=read(pipefd1[0], buf, MAX)) > 0)
        {
            for(int i=0;i<nb;++i)
                if(isalpha(buf[i]))
                    if(isupper(buf[i]))
                        buf[i]=tolower(buf[i]);
                    else
                        buf[i]=toupper(buf[i]);
            cout<<buf;
            cout<<endl;
        }
        close(pipefd1[0]);
    }
}