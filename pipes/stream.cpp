#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>
#include <iostream>
#include <ctype.h>
#include <string.h>
#include<fstream>
using namespace std;

int main()
{
    char *args[]={"tr","[:lower:]","[:upper:]",NULL};
    FILE *fin,*fout;
    fin=fopen("input.txt","r");
    fout=fopen("output.txt","w");
    if(fork()==0)
    {
        dup2(fileno(fin),0);
        dup2(fileno(fout),1);
        execvp(args[0],args);
    }
    else
        wait(NULL);
}