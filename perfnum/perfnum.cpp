#include<iostream>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
using namespace std;
void reaper(int signum)
{
    wait(NULL);
    exit(0);
}
void perfnum(int min,int max)
{
    cout<<"Perfect numbers in ("<<min<<","<<max<<"):\n";
    for(int i=min;i<=max;++i)
    {
        int sum=0;
        for(int j=1;j<=i/2;++j)
            if(i%j==0)
                sum+=j;
        if(sum==i)
            cout<<i<<"\t";
    }
    cout<<"\n";
    return;
}
int main()
{
    int min,max;
    cout<<"Enter the range:\n";
    cin>>min;
    cin>>max;
    if(fork()==0)
        perfnum(min,max);
    else
    {
        signal(SIGCHLD,reaper);
        while(1);
    }
}
