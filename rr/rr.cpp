#include<iostream>
#include<vector>
#include<algorithm>
#include<queue>
using namespace std;
struct jobs{
        int pid,at,bt,ct,tat,wt,rt;
};

bool comparator(jobs j1,jobs j2){return (j1.at<j2.at);}

class rr{
    vector <jobs> queues;
    queue <jobs> waitq;
    vector <jobs> compq;
    float avg_wt;
    int n,tq;
  public:
    void input()
    {
        cout<<"\nEnter the Time Quanta:";
        cin>>tq;
        cout<<"Enter the total number of jobs:";
        cin>>n;
        cout<<"Enter the jobs in the following format(pid,at,bt):\n";
        for(int i=0;i<n;++i)
        {
            jobs temp;
            cin>>temp.pid>>temp.at>>temp.bt;
            temp.rt=temp.bt;
            temp.ct=0;
            temp.tat=0;
            temp.wt=0;
            queues.push_back(temp);
        }
        avg_wt=0;
        sort(queues.begin(),queues.end(),comparator);
    }
    void update(int sys_clk)
    {
        while((queues.front().at<=sys_clk)&&(queues.size()!=0)) 
        {
            //cout<<"ss";
            waitq.push(queues.front());
            queues.erase(queues.begin());
        }
    }
    void schedule()
    {
        int sys_clk=0;
        while(compq.size()<n)
        {
            update(sys_clk);
            if(waitq.size()==0)
            {
                //cout<<sys_clk<<endl;
                sys_clk=sys_clk+1;   
                continue;
            }
            else if(waitq.front().rt<=tq)
            {
                sys_clk=sys_clk+waitq.front().rt;
                waitq.front().rt=0;
                waitq.front().ct=sys_clk;
                waitq.front().tat=waitq.front().ct-waitq.front().at;
                waitq.front().wt=waitq.front().tat-waitq.front().bt;
                avg_wt+=waitq.front().wt;
                compq.push_back(waitq.front());
                waitq.pop();
            }
            else
            {
                sys_clk=sys_clk+tq;
                waitq.front().rt-=tq;
                waitq.push(waitq.front());
                waitq.pop();
            }
        } 
    }
    void bout()
    {
        cout<<"PID\tAT\tBT\tCT\tTAT\tWT\n";
        for(int i=0;i<n;++i)
        {
            cout<<queues[i].pid<<"\t"<<queues[i].at<<"\t"<<queues[i].bt;
            cout<<"\t"<<queues[i].ct<<"\t"<<queues[i].tat<<"\t"<<queues[i].wt;
            cout<<endl;
        }
    }
    void output()
    {
        cout<<"PID\tAT\tBT\tCT\tTAT\tWT\n";
        for(int i=0;i<n;++i)
        {
            cout<<compq[i].pid<<"\t"<<compq[i].at<<"\t"<<compq[i].bt;
            cout<<"\t"<<compq[i].ct<<"\t"<<compq[i].tat<<"\t"<<compq[i].wt;
            cout<<endl;
        }
        cout<<"\nAvg:"<<avg_wt/n<<endl;
    }
};
int main()
{
    rr obj;
    obj.input();
    obj.bout();
    obj.schedule();
    obj.output();
    return 0;   
}