#include<iostream>
#include<unistd.h>
#include<wait.h>
using namespace std;
int main()
{
    int n;
    bool flag=0;
    std::cout.setf(std::ios::unitbuf);
    if(vfork()==0)
    {
        cout<<"Enter Number:";
        cin>>n;
        flag=1;
        _exit(0);
    }
    else
        if(flag)
        {
            cout<<n<<"\t";
            while(n!=1)
            {
                n=(n%2==0)?n/2:(3*n)+1;
                cout<<n<<"\t";
            }
            cout<<endl;
            _exit(0);
        }
        else
        {
        wait(NULL);
        _exit(0);
        }
}