#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

struct process{

	int pid, at, bt, pri;
	int tat, ct, wt; 
};

class prior_pe
{
	vector <process> a;
	int n;
  public :
	void input();
	void output();
	static bool cmp(process &x, process &y)
	{   return (x.at < y.at); }
	static bool cmp2(process &x, process &y)
	{   return (x.bt < y.bt); }	
	static bool cmp3(process &x, process &y)
	{   return (x.pid < y.pid); }	
	static bool cmp4(process &x, process &y)
	{   if (x.pri > y.pri)
		return true;
	    else if(x.pri == y.pri)
		return x.at < y.at;
	    else
 		return false; }			
};

void prior_pe::input()
{
  cout<<"Enter number of process\n";
  cin>>n;
  cout<<"Enter the process id, arrival time, burst time and priority\n";
  process x;
  for(int i =0; i<n; ++i)
  {
	cin>>x.pid>>x.at>>x.bt>>x.pri;
	a.push_back(x);
  }
}	


void prior_pe::output()
{
  vector <process> b, f, e;
  b = a;
  process x;
  sort(b.begin(), b.end(), cmp);
  int t, i, count = 0, max, j;
  t = b[0].at;
  int maxt = b[b.size()-1].at;
  while(b.size() != 0)
  {
 
	// All process have arrived	
	if(t >= maxt)
		break;
	sort(b.begin(), b.end(), cmp);
	max = b[0].pri;
	j = 0;
/*	for(auto x: b)
		cout<<x.pid<<" "<<x.at<<" "<<x.bt<<"\n"; */
	// Maximum Priority 
	for(i=0;i<b.size();i++)	
		if(max < b[i].pri && b[i].at <= t)
			{
				max = b[i].pri;
				j = i;
			}
	//cout<<"t at iter "<<count<<" : "<<t<<"\t";
	//cout<<"Min bt "<<min<<" j: "<<j<<"\t";
	// Next process arrival time
	/*	
	i = 0;
	while(b[i].at == b[0].at)
		i++;
	*/
	for(i=0;i<b.size();++i)
		if(b[i].at > t)
			break;
	//cout<<" i :"<<i<<"\n\n";
	
	// Remove the process or reduce the bt
	if(t + b[j].bt > b[i].at)
	{
		b[j].bt -= (b[i].at - t);
		t = b[i].at;
		f.push_back(b[j]);
	} 
	else
	{
		t += b[i].bt;
		b[j].bt = 0;
		b[j].ct = t;
		b[j].tat = b[j].ct - b[j].at;
		e.push_back(b[j]);
		f.push_back(b[j]);
		b.erase(b.begin() + j);
	}
	count++;
 	
  }
  
  cout<<"\nt : "<<t<<"\tB : \n";
	for(auto x: b)
		cout<<x.pid<<" "<<x.at<<" "<<x.bt<<"\n";
  cout<<"FINAL\n";
  for(auto y: f)
	cout<<y.pid<<" "<<y.at<<" "<<y.bt<<"\n";
  cout<<"\nE :";
  for(auto y: e)
	cout<<y.pid<<" "<<y.at<<" "<<y.bt<<"\n";
  // SJF for remaning process 
  sort(b.begin(), b.end(), cmp4);
  for(i=0;i<b.size();++i)
  {
	b[i].ct = t + b[i].bt;
	t += b[i].bt;
	b[i].tat = b[i].ct - b[i].at; 
  }
 /*
  cout<<"\nPID  -  AT  - BT   - CT  - TAT  - WT \n";	
  for(i = 0;i<b.size();++i)
	cout<<b[i].pid<<" "<<b[i].at<<" "<<b[i].bt<<" "<<b[i].ct<<" "<<" "<<b[i].tat<<" "<<" "<<b[i].wt<<"\n";
  cout<<"\ndone\n"; */
  for(auto x: e)
	b.push_back(x);
  sort(b.begin(), b.end(), cmp3);
  sort(a.begin(), a.end(), cmp3);
  for(i=0;i<b.size();++i)
 {
		b[i].at = a[i].at;
		b[i].bt = a[i].bt;
		b[i].wt = b[i].tat - b[i].bt;
	
 }
  a = b ;
  sort(a.begin(), a.end(), cmp);
  cout<<"\nPID  -  AT  - BT   -   PRI   - CT  - TAT  - WT \n";	
  for(int i = 0;i<n;++i)
	cout<<a[i].pid<<" "<<a[i].at<<" "<<a[i].bt<<" "<<a[i].pri<<" "<<a[i].ct<<" "<<" "<<a[i].tat<<" "<<" "<<a[i].wt<<"\n";
   	
}



int main()
{
  class prior_pe b;
  b.input();
  cout<<"\n";
  b.output(); 
}

/*
	6
	1 6 1 
	2 3 3 
	3 4 6 
	4 1 5 
	5 2 2 
	6 5 1 

4
1 0 8
2 1 4
3 2 9
4 3 5

6
1 1 4 4 
2 2 2 5 
3 2 3 7 
4 3 5 8 
5 3 1 5 
6 4 2 6 

*/
